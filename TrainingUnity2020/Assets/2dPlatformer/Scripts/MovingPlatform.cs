﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    private Vector3 _finishPos;
    private float speed = 0.5f;
    private Vector3 _startPos;
    [SerializeField]
    private float _trackPercent = 0;
    private int _direction = 1;

    private void Start()
    {
        _startPos = transform.position;
        _finishPos = transform.position + new Vector3(5, 0, 0);
    }
    private void Update()
    {
        _trackPercent += _direction * speed * Time.deltaTime;
        float x = (_finishPos.x - _startPos.x) * _trackPercent + _startPos.x;
        float y = (_finishPos.y - _startPos.y) * _trackPercent + _startPos.y;
        transform.position = new Vector3(x, y, _startPos.z);
        if((_direction == 1 && _trackPercent > .9f) || (_direction == -1 && _trackPercent < 0.1f))
        {
            _direction *= -1;
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, _finishPos);
    }

}
