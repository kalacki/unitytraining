﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlatformerPlayer : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _jumpForce;
    private Rigidbody2D _rb;
    private Animator _anim;
    private BoxCollider2D _box;
    private Vector3 max;
    private Vector3 min;
    private Vector2 corner1;
    private Vector2 corner2;
    private float deltaX;
    private Vector2 movement;
    private bool grounded;
    private void Awake()
    {
        UIController.Instance.onSpeedChanged += ChangingOfValues; 
    }

    private void ChangingOfValues(object sender, EventArgs e)
    {
        _speed += (float)sender;
    }

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _anim = GetComponent<Animator>();
        _box = GetComponent<BoxCollider2D>();
    }
    private void Update()
    {
        Movement();
    }

    private void Movement()
    {

        deltaX = Input.GetAxis("Horizontal") * _speed * Time.fixedDeltaTime;
        movement = new Vector2(deltaX, _rb.velocity.y);
        _rb.velocity = movement;
        max = _box.bounds.max;
        min = _box.bounds.min;
        corner1 = new Vector2(max.x, min.y - 0.1f);
        corner2 = new Vector2(min.x, min.y - 0.2f);
        Collider2D hit = Physics2D.OverlapArea(corner1, corner2);
        grounded = false;
        if (hit != null)
        {
            grounded = true;
        }
        _rb.gravityScale = grounded && deltaX == 0 ? 0 : 4;
        if (grounded && Input.GetKeyDown(KeyCode.Space))
        {
            _rb.AddForce(Vector2.up * _jumpForce, ForceMode2D.Impulse);
        }
        MovingPlatform platform = null;

        if (hit != null)
        {
            platform = hit.GetComponent<MovingPlatform>();
        }
        if (platform != null)
        {
            transform.parent = platform.transform;
        }
        else
        {
            transform.parent = null;
        }
        _anim.SetFloat("speed", Mathf.Abs(deltaX));

        Vector3 psScale = Vector3.one;

        if (platform != null)
        {
            psScale = platform.transform.localScale;
        }
        if (!Mathf.Approximately(deltaX, 0))
        {
            transform.localScale = new Vector3(Mathf.Sign(deltaX) / psScale.x,1/ psScale.y, 1);
        }
    }
}
//transform.localScale = new Vector3(Mathf.Sign(deltaX) / psScale.x, 1 / psScale.y, 1);