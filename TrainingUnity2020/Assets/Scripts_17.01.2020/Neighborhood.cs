﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SphereCollider))]
public class Neighborhood : MonoBehaviour
{
    [Header("Set Dynamically")]
    [SerializeField]
    private List<Bird> _neighboors;
    [SerializeField]
    private SphereCollider _coll;
    [SerializeField]
    private float _colliderRadiusCheck;
    private Vector3 _avgPos;
    private Vector3 _avgVel;
    private Vector3 _avgClosePos;

    public Vector3 AvgPos
    {
        get
        {
            if (_neighboors.Count == 0)
            {
                return _avgPos;
            }
            for (int i = 0; i < _neighboors.Count; i++)
            {
                _avgPos += _neighboors[i].Pos;
            }
            _avgPos /= _neighboors.Count;
            return _avgPos;
        }
    }
    public Vector3 AvgVelocity
    {
        get
        {
            if (_neighboors.Count == 0)
            {
                return _avgVel;
            }
            for (int i = 0; i < _neighboors.Count; i++)
            {
                _avgVel += _neighboors[i].Velocity;
            }
            _avgVel /= _neighboors.Count;
            return _avgVel;
        }
    }
    public Vector3 AverageClosePos
    {
        get
        {
            Vector3 delta;
            int nearCount = 0;
            for(int i=0; i<_neighboors.Count; i++)
            {
                delta = _neighboors[i].Pos - transform.position;
                if (delta.magnitude <= Spawner._spawner.ColliderDistance)
                {
                    _avgClosePos += _neighboors[i].Pos;
                    nearCount++;
                }
            }
            if(nearCount == 0)
            {
                return _avgClosePos;
            }
            _avgClosePos /= nearCount;
            return _avgClosePos;
        }
    }
    private void Start()
    {
        _neighboors = new List<Bird>();
        _coll = GetComponent<SphereCollider>();
        _coll.radius = Spawner._spawner.NeighborDistance / 2;
    }
    private void FixedUpdate()
    {
        if (_coll.radius != Spawner._spawner.NeighborDistance/2)
        {
            _coll.radius = Spawner._spawner.NeighborDistance / 2;
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Bird b = other.GetComponent<Bird>();
        if (b != null)
        {
            if (_neighboors.IndexOf(b) == -1)
            {
                _neighboors.Add(b);
                print("Trigger entered!");
            }
        }
        
    }
    private void OnTriggerExit(Collider other)
    {
        Bird b = other.GetComponent<Bird>();
        if (b != null)
        {
            if (_neighboors.IndexOf(b) != -1)
            {
                _neighboors.Remove(b);
                print("Trigger exited!");
            }
        }
    }



}
