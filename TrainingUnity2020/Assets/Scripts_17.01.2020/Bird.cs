﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(TrailRenderer))]

public class Bird : MonoBehaviour
{
    [SerializeField]
    private Neighborhood _neighboorhood;
    private Rigidbody _rigidbbody;
    private TrailRenderer _trailRenderer;
    private bool _isAttracted;
    private float SpawnerAttractPushDistance;
    private float SpawnerAttractPush;
    private float SpawnerAttractPull;
    private float SpawnerColliderAvoid;
    private float SpawnerVelocityMatching;
    private float SpawnerFlockCentering;
    private float _spawnerVelocity;
    private Vector3 _velAttract;
    private Vector3 _delta;
    private Vector3 _velAvoid;
    private Vector3 _tooClosePos;
    private Vector3 _velAlign;
    private Vector3 _velCenter;
    public Vector3 Pos
    {
        get { return transform.position; }
        set { transform.position = value; }
    }
    public Vector3 Velocity
    {
        get { return _rigidbbody.velocity; }
        private set { _rigidbbody.velocity = value; }
    }
    private void Awake()
    {
        _rigidbbody = GetComponent<Rigidbody>();
        _trailRenderer = GetComponent<TrailRenderer>();
        Pos = UnityEngine.Random.insideUnitSphere * Spawner._spawner.SpawnRadius;
        Velocity = UnityEngine.Random.onUnitSphere * Spawner._spawner.Velocity;
        LookAhead();
        RandomColorSetting();
        SpawnerAttractPushDistance = Spawner._spawner.AttractPushDistance;
        //_spawnerVelocity = Spawner._spawner.Velocity;
        SpawnerAttractPush = Spawner._spawner.AttractPush;
        SpawnerAttractPull = Spawner._spawner.AttractPull;
        SpawnerColliderAvoid = Spawner._spawner.ColliderAvoid;
        SpawnerVelocityMatching = Spawner._spawner.VelocityMatching;
        SpawnerFlockCentering = Spawner._spawner.FlockCentering;
    }

    private void LookAhead()
    {
        transform.LookAt(Pos + _rigidbbody.velocity);
    }
    private void RandomColorSetting()
    {
        Renderer[] rend = gameObject.GetComponentsInChildren<Renderer>();
        Color rndColor = new Color(UnityEngine.Random.value,UnityEngine.Random.value, UnityEngine.Random.value);
        foreach (Renderer r in rend)
        {
            r.material.color = rndColor;
        }
        _trailRenderer.material.SetColor("_Color", rndColor);
    }
    private void FixedUpdate()
    {
        _spawnerVelocity = Spawner._spawner.Velocity;
        _tooClosePos = _neighboorhood.AverageClosePos;
        if(_tooClosePos != Vector3.zero)
        {
            _velAvoid = Pos - _tooClosePos;
            _velAvoid.Normalize();
            _velAvoid *= _spawnerVelocity;
        }
        _velAlign = _neighboorhood.AvgVelocity;
        if(_velAlign != Vector3.zero)
        {
            _velAlign.Normalize();
            _velAlign *= _spawnerVelocity;
        }
        _velCenter = _neighboorhood.AvgPos;
        if(_velCenter != Vector3.zero )
        {
            _velCenter -= transform.position;
            _velCenter.Normalize();
            _velCenter *= _spawnerVelocity;
        }

        _delta = Attractor.POSprop - Pos;
        _isAttracted = (_delta.magnitude > SpawnerAttractPushDistance);
        _velAttract = _delta * _spawnerVelocity;

       
        if(_velAvoid != Vector3.zero)
        {
            Velocity = Vector3.Lerp(Velocity, _velAlign, SpawnerColliderAvoid * Time.fixedDeltaTime);
        }
        else
        {
            if(_velAlign != Vector3.zero)
            {
                Velocity = Vector3.Lerp(Velocity, _velAlign, SpawnerVelocityMatching * Time.fixedDeltaTime);
            }
            if(_velCenter != Vector3.zero)
            {
                Velocity = Vector3.Lerp(Velocity, _velCenter, SpawnerFlockCentering * Time.fixedDeltaTime);
            }
            if(_velAttract != Vector3.zero)
            {
                if (_isAttracted)
                {
                    Velocity = Vector3.Lerp(Velocity, _velAttract, SpawnerAttractPull * Time.fixedDeltaTime);
                }
                else
                {
                    Velocity = Vector3.Lerp(Velocity, -_velAttract, SpawnerAttractPush * Time.fixedDeltaTime);
                }
            }
        }
        Velocity = Velocity.normalized * _spawnerVelocity;
        LookAhead();
    }
}
