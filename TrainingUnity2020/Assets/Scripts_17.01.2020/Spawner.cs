﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    static public Spawner _spawner;
    static public List<Bird> _birds;
    [Header("Sit in Inspector: Spawning")]
    [SerializeField]
    private GameObject _birdPrefab;
    [SerializeField]
    private Transform _birdAnchor;
    [SerializeField]
    private int _numBirds;
    [SerializeField]
    private float _spawnRadius;
    [SerializeField]
    private float _spawnDelay;
    [Header("Set in Inspector: Birds")]
    [SerializeField]
    private float _velocity;
    [SerializeField]
    private float _neighborDistance;
    [SerializeField]
    private float _colliderDistance;
    [SerializeField]
    private float _velocityMatching;
    [SerializeField]
    private float _flockCentering;
    [SerializeField]
    private float _colliderAvoid;
    [SerializeField]
    private float _attractPull;
    [SerializeField]
    private float _attractPush;
    [SerializeField]
    private float _attractPushDistance;
    private int counterForBirdsNames = 1;
    public float AttractPull
    {
        get { return _attractPull; }
        private set { }
    }
    public float AttractPush
    {
        get { return _attractPush; }
        private set { }
    }

    public float AttractPushDistance
    {
        get { return _attractPushDistance; }
        private set { }
    }
    public float SpawnRadius
    {
        get { return _spawnRadius; }
        private set { }
    }
    public float Velocity
    {
        get { return _velocity; }
        private set { }
    }
    public float NeighborDistance
    {
        get { return _neighborDistance; }
        private set { }
    }
    public float ColliderDistance
    {
        get { return _colliderDistance; }
        private set { }
    }
    public float ColliderAvoid
    {
        get { return _colliderAvoid; }
        private set { }
    }

    public float VelocityMatching
    {
        get { return _velocityMatching; }
        private set { }
    }
    public float FlockCentering
    {
        get { return _flockCentering; }
        private set { }
    }

    private void Awake()
    {
        _spawner = this;
        _birds = new List<Bird>();
        InstantiateBird();
    }
    private void InstantiateBird()
    {
        GameObject go = Instantiate(_birdPrefab);
        go.name = "Bird " + counterForBirdsNames;
        Bird b = go.GetComponent<Bird>();
        b.transform.SetParent(_birdAnchor);
        _birds.Add(b);
        if (_birds.Count < _numBirds)
        {
            counterForBirdsNames++;
            Invoke("InstantiateBird", _spawnDelay);
        }
    }
}
