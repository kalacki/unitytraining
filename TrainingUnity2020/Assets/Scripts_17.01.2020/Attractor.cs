﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour
{
    private static Vector3 POS;
    private static Vector3 tempPos;
    private Vector3 scale;

    [Header("Set in Inspector")]
    [SerializeField]
    private float radius;
    [SerializeField]
    private float xPhase;
    [SerializeField]
    private float yPhase;
    [SerializeField]
    private float zPhase;

    static public Vector3 POSprop
    {
        get { return tempPos; }
        private set { }
    }


    private void Awake()
    {
        print("Awake!");
        tempPos = Vector3.zero;
        scale = transform.localScale;
    }
    private void FixedUpdate()
    {
        CalculateTheSins();
        transform.position = tempPos;
        POS = tempPos;
    }
    private void CalculateTheSins()
    {
        tempPos.x = Mathf.Sin(xPhase * Time.time) * radius * scale.x;
        tempPos.y = Mathf.Sin(yPhase * Time.time) * radius * scale.y;
        tempPos.z = Mathf.Sin(zPhase * Time.time) * radius * scale.z;
    }
}
