﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitCamera : MonoBehaviour
{
    [SerializeField]
    private Transform _player;
    [SerializeField]
    private float rotSpeed;
    private float _rotY;
    private Vector3 _offset;
    [SerializeField]
    private float _mouseSensivity;
    private Camera _camera;
    private float _rotX;

    private void Start()
    {
        _rotY = transform.eulerAngles.y;
        _rotX = transform.eulerAngles.x;
        _offset = _player.position - transform.position;
        _camera = GetComponent<Camera>();
    }
    private void LateUpdate()
    {
        //float horInput = Input.GetAxis("Horizontal");
        //if (horInput != 0)
        //{
        //    _rotY += horInput * rotSpeed;
        //}
        //else
        _rotY += Input.GetAxis("Mouse X") * rotSpeed * _mouseSensivity;
        _rotX -= Input.GetAxis("Mouse Y") * rotSpeed * _mouseSensivity;
        Quaternion rotation = Quaternion.Euler(_rotX, _rotY, 0);
        transform.position = _player.position - (rotation * _offset);
        transform.LookAt(_player);
    }
}
