﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneControllerMemoryGame : MonoBehaviour
{
    private const int _gridRows = 2;
    private const int _gridCols = 4;
    private const float _offsetX = 2f;
    private const float _offsetY = 2.5f;
    private int _score = 0;


    [SerializeField]
    private MemoryCard _originalCard;
    [SerializeField]
    private Sprite[] _images;
    [SerializeField]
    private Text _scoreText;

    private MemoryCard _firstRevealed;
    private MemoryCard _secondRevealed;

    public bool CanReveal
    {
        get
        {
            return _secondRevealed == null;
        }
    }
    public void CardRevealed(MemoryCard card)
    {
        if(_firstRevealed == null)
        {
            _firstRevealed = card;
        }
        else
        {
            _secondRevealed = card;
            StartCoroutine(CheckMatch());
        }
    }

    private IEnumerator CheckMatch()
    {
        if(_firstRevealed.ID == _secondRevealed.ID)
        {
            _score++;
            _scoreText.text = $"Score: {_score}";
        }
        else
        {
            yield return new WaitForSeconds(1f);
            _firstRevealed.Unreveal();
            _secondRevealed.Unreveal();
        }
        _firstRevealed = null;
        _secondRevealed = null;
    }

    private void Start()
    {
        Vector3 startPos = _originalCard.transform.position;
        int[] numbers = { 0, 0, 1, 1, 2, 2, 3, 3 };
        numbers = ShuffleArray(numbers);
        for (int i = 0; i < _gridCols; i++)
        {
            for (int j = 0; j < _gridRows; j++)
            {
                MemoryCard card;
                card = Instantiate(_originalCard);
                card.SceneController = this;
                int index = j * _gridCols + i;
                int id = numbers[index];
                card.SetCard(id, _images[id]);
                float posX = (_offsetX * i) + startPos.x;
                float posY = -(_offsetY * j) + startPos.y;
                card.transform.position = new Vector3(posX, posY, startPos.z);
            }
        }

    }
    private int[] ShuffleArray(int[] numbers)
    {
        
        for(int i = 0; i< numbers.Length; i++)
        {
            int tmp = numbers[i];
            int r = UnityEngine.Random.Range(i, numbers.Length);
            numbers[i] = numbers[r];
            numbers[r] = tmp;
        }
        return numbers;
    }
}
