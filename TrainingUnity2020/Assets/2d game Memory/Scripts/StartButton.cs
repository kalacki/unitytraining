﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour,  IPointerDownHandler, IPointerUpHandler
{
    private Button _button;

    private void Start()
    {
        _button = GetComponent<Button>();
    }

  
    public void OnPointerUp(PointerEventData eventData)
    {
        _button.transform.localScale = Vector3.one;
        SceneRestart();
    }

    private void SceneRestart()
    {
        var currentScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(currentScene.name);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _button.transform.localScale = new Vector2(1.1f, 1.1f);
    }
}
