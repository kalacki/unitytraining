﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryCard : MonoBehaviour
{
    [SerializeField]
    private GameObject _cardBack;
    [SerializeField]
    private SceneControllerMemoryGame _sceneController;
    [SerializeField]
    private int _id;
    public SceneControllerMemoryGame SceneController
    {
        set
        {
            _sceneController = value;
        }
    }
    public int ID
    {
        get
        {
            return _id;
        }
        private set
        {
            _id = value;
        }
    }
    public void SetCard(int id, Sprite image)
    {
        ID = id;
        GetComponent<SpriteRenderer>().sprite = image;
    }
    
    private void OnMouseDown()
    {
        if(_cardBack.activeSelf && _sceneController.CanReveal)
        {
            _cardBack.SetActive(false);
            _sceneController.CardRevealed(this);
        }
    }
    public void Unreveal()
    {
        _cardBack.SetActive(true);
    }

}

