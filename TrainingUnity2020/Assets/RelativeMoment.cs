﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class RelativeMoment : MonoBehaviour
{
    [SerializeField]
    private Transform _camera;
    private CharacterController _charController;
    private Vector3 movement;
    [SerializeField]
    private float rotSpeed;
    [SerializeField]
    private float moveSpeed;
    [SerializeField]
    private float jumpSpeed;
    private const float gravity = -9.8f;
    private float terminalVelocity = -10.0f;
    private float minFall = -1.5f;
    [SerializeField]
    private float vertSpeed;
    private ControllerColliderHit _contact;
    private Animator _anim;
    private void Awake()
    {
        _charController = GetComponent<CharacterController>();
        vertSpeed = minFall;
        _anim = GetComponent<Animator>();
    }
    private void Update()
    {
        movement = Vector3.zero;
        float horInput = Input.GetAxis("Horizontal");
        float vertInput = Input.GetAxis("Vertical");
        if (horInput != 0 || vertInput != 0)
        {
            movement.x = horInput * moveSpeed;
            movement.z = vertInput * moveSpeed;
            movement = Vector3.ClampMagnitude(movement, moveSpeed);
            Quaternion tmp = _camera.rotation;
            _camera.eulerAngles = new Vector3(0, _camera.eulerAngles.y, 0);
            movement = _camera.TransformDirection(movement);
            _camera.rotation = tmp;
            Quaternion direction = Quaternion.LookRotation(movement);
            transform.rotation = Quaternion.Slerp(transform.rotation, direction, rotSpeed * Time.deltaTime);
        }
        _anim.SetFloat("Speed", movement.sqrMagnitude);
        bool hitGround = false;
        RaycastHit myhit;
        if (vertSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out myhit))
        {
            float check = (_charController.height + _charController.radius) / 1.9f;
            hitGround = myhit.distance <= check;
        }
        if (hitGround)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                vertSpeed = jumpSpeed;
                _anim.SetBool("Jumping", true);
            }
            else
            {
                vertSpeed = minFall;
                _anim.SetBool("Jumping", false);
            }
        }
        else
        {
            vertSpeed += gravity * 5 * Time.deltaTime;
            if (vertSpeed < terminalVelocity)
            {
                vertSpeed = terminalVelocity;
            }
            if(_contact !=null)
            {
                _anim.SetBool("Jumping", true);
            }
            if (_charController.isGrounded)
            {
                if (Vector3.Dot(movement, _contact.normal) < 0)
                {
                    movement = _contact.normal * moveSpeed;
                }
                else
                {
                    movement += _contact.normal * moveSpeed;
                }
            }
        }
        movement.y = vertSpeed;
        movement *= Time.deltaTime;
        _charController.Move(movement);
    }
    private void OnControllerColliderHit(ControllerColliderHit hit)
    {
        _contact = hit;
    }
}
