﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChangeDevice : MonoBehaviour
{
    public void Operate()
    {
        Color randomCol = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
        GetComponent<Renderer>().material.color = randomCol;
    }
}
