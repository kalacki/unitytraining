﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class DoorOpenDevice : MonoBehaviour
{
    [SerializeField]
    private Vector3 dPos;
    [SerializeField]
    private Animator _anim;
    private bool _open = false;
    private void Awake()
    {
        _anim.GetComponent<Animator>();
    }
    public void Operate()
    {
        if(_open)
        {
            Vector3 pos = transform.position - dPos;
            transform.position = pos;
        }
        else
        {
            _anim.SetBool("opening",true);
        }
    }

}
