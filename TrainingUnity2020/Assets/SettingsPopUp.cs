﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsPopUp : MonoBehaviour
{
    [SerializeField]
    private Button _closeButton;
    [SerializeField]
    private Slider _slider;
    [SerializeField]
    private InputField _inputField;
    public event EventHandler onSpeedValue;
    private void Awake()
    {
        _closeButton.onClick.AddListener(Close);
        _inputField.onValueChanged.AddListener(OnSubmitName);
        _slider.onValueChanged.AddListener(OnSpeedValue);
    }

    private void OnSpeedValue(float speed)
    {
        onSpeedValue.Invoke(speed,EventArgs.Empty);
    }

    private void OnSubmitName(string name)
    {
        Debug.Log($"Name: {name}");
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }
    public void Close()
    {
        gameObject.SetActive(false);
    }


    
}
