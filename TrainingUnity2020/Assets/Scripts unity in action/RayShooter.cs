﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
[RequireComponent(typeof(Camera))]
public class RayShooter : MonoBehaviour
{
    private Camera _camera;
    private int _size;
    private float _crosshairPosX;
    private float _crosshairPosY;
    void Start()
    {
        _camera = GetComponent<Camera>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        _size = 12;
        _crosshairPosX = _camera.pixelWidth / 2 - _size / 4;
        _crosshairPosY = _camera.pixelHeight / 2 - _size / 2;
    }

    void Update()
    {
        Shooting();
    }
    //private void OnGUI()
    //{
    //    GUI.Label(new Rect(_crosshairPosX, _crosshairPosY, _size, _size), "+");
    //}
    private void Shooting()
    {
        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);
            Ray ray = _camera.ScreenPointToRay(point);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                GameObject hitObject = hit.transform.gameObject;
                ReactiveTarget target = hitObject.GetComponent<ReactiveTarget>();
                if(target != null)
                {
                    Debug.Log("Target hit!");
                    target.ReactToHit();
                }
                else
                {
                    StartCoroutine(ShootingIndicator(hit.point));
                }
            }
        }
    }

    private IEnumerator ShootingIndicator(Vector3 point)
    {
        GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        sphere.transform.position = point;
        yield return new WaitForSeconds(1.0f);

        Destroy(sphere);
    }
}
