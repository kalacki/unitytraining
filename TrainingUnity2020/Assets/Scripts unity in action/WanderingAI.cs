﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WanderingAI : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _obstacleRange;
    [SerializeField]
    private GameObject _fireballPrefab;
    private GameObject _fireball;
    private float angle;
    private bool _isAlive;
    public bool IsAlive
    {
        get
        {
            return _isAlive;
        }
        set
        {
            _isAlive = value;
        }
    }
    private void Start()
    {
        _isAlive = true;
    }
    private void Update()
    {
        if (_isAlive)
        {
            Moving();
        }
    }
    private void Moving()
    {
        transform.Translate(0, 0, _speed * Time.deltaTime);
        Ray ray = new Ray(transform.position, transform.forward);
        RaycastHit hit;
        Debug.DrawRay(transform.position, ray.direction * _obstacleRange, Color.red);

        if (Physics.SphereCast(ray, 0.75f, out hit))
        {
            GameObject hitObject = hit.transform.gameObject;
            if (hitObject.GetComponent<PlayerCharacter>())
            {
                if (_fireball == null)
                {
                    _fireball = Instantiate(_fireballPrefab);
                    _fireball.transform.position = transform.TransformPoint(Vector3.forward * 1.5f);
                    _fireball.transform.rotation = transform.rotation;
                }
            }
            else if (hit.distance < _obstacleRange)
            {
                angle = Random.Range(-110f, 110f);
                transform.Rotate(0, angle, 0);
            }
        }
    }
}
