﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public enum RotationAxes
    {
        MouseXAndY = 0,
        MouseX = 1,
        MouseY = 2
    }
    [SerializeField]
    private RotationAxes _axes = RotationAxes.MouseXAndY;
    [SerializeField]
    private float _horizontalSensivity;
    [SerializeField]
    private float _verticalSensivity;
    private float _minimumVert = -45.0f;
    private float _maximumVert = 45.0f;
    private float _rotationX = 0;
    private float _rotationY = 0;
    private float _delta = 0;
    

    private void Start()
    {
        Rigidbody body = GetComponent<Rigidbody>();
        if(body != null)
        {
            body.freezeRotation = true;
        }
    }

    private void Update()
    {
        if(_axes == RotationAxes.MouseX)
        {
            transform.Rotate(0, Input.GetAxis("Mouse X")*_horizontalSensivity, 0);
        }
        else if(_axes == RotationAxes.MouseY)
        {
            _rotationX -= Input.GetAxis("Mouse Y") * _verticalSensivity;
            _rotationX = Mathf.Clamp(_rotationX, _minimumVert, _maximumVert);
            _rotationY = transform.localEulerAngles.y;
            transform.localEulerAngles = new Vector3(_rotationX, _rotationY, 0);
        }
        else
        {
            _rotationX -= Input.GetAxis("Mouse Y") * _verticalSensivity;
            _rotationX = Mathf.Clamp(_rotationX, _minimumVert, _maximumVert);
            _rotationY += Input.GetAxis("Mouse X") * _horizontalSensivity;
            transform.localEulerAngles = new Vector3(_rotationX, _rotationY, 0);
        }
    }

}
