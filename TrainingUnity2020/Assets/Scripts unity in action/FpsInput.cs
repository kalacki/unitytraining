﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
[RequireComponent(typeof(CharacterController))]
[AddComponentMenu("Control Script/FPS Input")]
public class FpsInput : MonoBehaviour
{
    private CharacterController _char;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private GameObject _flashlight;
    private float deltaX;
    private float deltaZ;
    private Vector3 _movement;
    private float _gravity = -9.8f;
    private bool _isEnabled;

    private void Start()
    {
        _isEnabled = false;
        _char = GetComponent<CharacterController>();
    }
    private void Update()
    {
        Movement();
        Flashlight();
    }

    private void Flashlight()
    {
        if (Input.GetKeyDown(KeyCode.F) && !_isEnabled)
        {
            _flashlight.SetActive(true);
            _isEnabled = true;
        }
        else if (Input.GetKeyDown(KeyCode.F) && _isEnabled)
        {
            _flashlight.SetActive(false);
            _isEnabled = false;
        }
    }

    private void Movement()
    {
        deltaX = Input.GetAxis("Horizontal") * _speed;
        deltaZ = Input.GetAxis("Vertical") * _speed;
        _movement = new Vector3(deltaX, 0, deltaZ);
        _movement = Vector3.ClampMagnitude(_movement, _speed);
        _movement.y = _gravity;
        _movement *= Time.deltaTime;
        _movement = transform.TransformDirection(_movement);
        _char.Move(_movement);
    }
}
