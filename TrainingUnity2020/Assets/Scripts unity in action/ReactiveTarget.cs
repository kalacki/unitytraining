﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent (typeof(Animator))]
public class ReactiveTarget : MonoBehaviour
{
    private Animator _animator;
    void Start()
    {
        _animator = GetComponent<Animator>();
    }
    internal void ReactToHit()
    {
        WanderingAI beh = GetComponent<WanderingAI>();
        if(beh != null)
        {
            beh.IsAlive = false;
        }
        StartCoroutine(Die());
    }

    private IEnumerator Die()
    {
        float deathTime = 1;
        transform.Rotate(-75, 0, 0, Space.Self);
        yield return new WaitForSeconds(deathTime);
        Destroy(gameObject);
    }
}
