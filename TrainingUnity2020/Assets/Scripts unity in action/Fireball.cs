﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem _ps;
    [SerializeField]
    private float _speed;
    [SerializeField]
    private float _damage;
    private void Start()
    {
        _ps.GetComponent<ParticleSystem>().Play();
        Destroy(gameObject, 5.0f);
    }
    private void Update()
    {
        transform.Translate(0, 0, _speed * Time.deltaTime);
    }
    private void OnTriggerEnter(Collider other)
    {
        PlayerCharacter player = other.GetComponent<PlayerCharacter>();
        if(player != null)
        {
            Debug.Log("Player Hit!");
            player.Hurt((int)_damage);
            Destroy(gameObject);
        }
    }
}
