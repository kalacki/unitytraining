﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField]
    private GameObject _enemyPrefab;
    private GameObject _enemy;
        
    void Update()
    {
        if(_enemy == null)
        {
            InstantiateEnemy();
        }
    }

    private void InstantiateEnemy()
    {
        _enemy = Instantiate(_enemyPrefab) as GameObject;
        _enemy.transform.position = new Vector3(0, 1, 0);
        float angle = UnityEngine.Random.Range(0, 360f);
        _enemy.transform.Rotate(0, angle, 0);
    }
}
