﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [SerializeField]
    private Text _scoreLabel;
    [SerializeField]
    private Button _settingsButton;
    [SerializeField]
    private SettingsPopUp _popUp;
    
    public static UIController Instance;
    public event EventHandler onSpeedChanged;
    private void Awake()
    {
        Instance = this;
        _settingsButton.onClick.AddListener(OnOpenSettings);
        _popUp.Close();
    }
    
    private void Update()
    {
        _scoreLabel.text =Time.realtimeSinceStartup.ToString();
    }
    public void OnOpenSettings()
    {
        _popUp.Open();
        _popUp.onSpeedValue += _popUp_onSpeedValue;
    }

    private void _popUp_onSpeedValue(object sender, EventArgs e)
    {
        onSpeedChanged.Invoke(sender, e);
    }
}
